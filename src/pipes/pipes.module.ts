import { NgModule } from '@angular/core';
import { LimitarTextoPipe } from './limitar-texto/limitar-texto';
@NgModule({
	declarations: [LimitarTextoPipe],
	imports: [],
	exports: [LimitarTextoPipe]
})
export class PipesModule {}
