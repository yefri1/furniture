import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController,ToastController } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import { Util } from '../../recursos/util';
import { Storage } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';


@Component({
	selector: 'page-chat',
	templateUrl: 'chat.html',
})
export class ChatPage {
	@ViewChild(Content) content: Content;

	usuario: any;
	messages = [];
	message = '';
	salir = false;	


	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private socket: Socket,
		private recursos: Util,
		private storage: Storage,
		private keyboard: Keyboard,
		private alertCtrl: AlertController,
		private toastCtrl: ToastController) {
		this.socket.connect();
		this.validarUsuario();
		//this.createFirstMessage();

		//this.storage.set('productoPorDefecto', this.productos);

		this.keyboard.onKeyboardShow().subscribe(() => {
			this.scroolBottom(1000);
		});

		this.getMessages().subscribe((message: any) => {
			this.messages.push(message);

			console.log(message);
		});

		this.scroolBottom(1000);
	}

	

	createFirstMessage() {
		let message = this.navParams.get('message');

		let objectMessage = {
			text: message,
			author: 'Frank',
			data: []
		}

		this.messages.push(objectMessage);
		this.socket.emit('new-message', objectMessage);
	}

	validarUsuario() {
		if (this.validarSesion()) {
			this.usuario = JSON.parse(localStorage.getItem('usuario'));
		} else {
			this.usuario = null;
		}

	}
	ionViewCanLeave() {
		if(!this.salir) {
			let alertPopup = this.alertCtrl.create({
				title: 'Estas a punto de salir...',
				message: '¿Estas seguro que quieres salir? Perderas toda la conversación',
				buttons: [{
					text: 'Si',
					handler: () => {
						alertPopup.dismiss().then(() => {
							this.salir = true;	
							
							this.socket.disconnect();

							this.disconnect().subscribe((message: any) => {
								this.presentToast(message);
								console.log(message);
							});			
							
							this.navCtrl.pop();	
						});
	
						return false;
					},
	
				},
				{
					text: 'No',
					handler: () => {
						// need to do something if the user stays?
					}
				}]
			});		
	
			alertPopup.present();

			return false;
		}

		return true;
		
	}

	presentToast(mensaje) {
		let toast = this.toastCtrl.create({
		  message: mensaje,
		  duration: 3000,
		  position: 'bottom'
		});
	  
		toast.onDidDismiss(() => {
		  
		});
	  
		toast.present();
	}

	ionViewDidEnter() {
		this.content.scrollToBottom(300);//300ms animation speed
	}

	validarSesion() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));
		let token = localStorage.getItem('token');

		return usuario && token ? true : false;
	}

	sendMessage() {
		this.scroolBottom(100);

		let objectMessage = {
			text: this.message,
			author: this.usuario ? this.usuario.nombre : null,
			data: []
		}

		this.socket.emit('new-message', objectMessage);
		this.messages.push(objectMessage);

		this.message = ''
	}

	getMessages() {
		let ob = new Observable(observer => {
			this.socket.on('messages', data => {
				observer.next(data);
			});
		});

		return ob;
	}

	disconnect() {
		let ob = new Observable(observer => {
			this.socket.on('disconnect', data => {
				observer.next(data);
			});
		});

		return ob;
	}

	scroolBottom(time) {
		setTimeout(() => {
			this.content.scrollToBottom(300);//300ms animation speed			
		}, time);
	}

	getMessagess() {
		this.storage.get('messages').then((val) => {
			console.log('Your age is', val);
		});
	}



}
