import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, Platform, LoadingController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SesionProvider } from '../../providers/sesion/sesion';

@Component({
  selector: 'page-detalle-usuario',
  templateUrl: 'detalle-usuario.html',
})

export class DetalleUsuarioPage {

  email: string;
  pass: string;
  nombres: string;
  apellidos: string;
  fecha: string;

  loading: any;

  usuario: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public camara: Camera,
    public crop: Crop,
    private file: File,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private sesion: SesionProvider) {
    this.usuario = JSON.parse(localStorage.getItem('usuario'));

    console.log(this.usuario);
    this.setUser();
  }

  setUser() {
    this.email = this.usuario.email;
    this.nombres = this.usuario.nombre;
    this.apellidos = this.usuario.apellido;
    this.fecha = this.usuario.fechaNacimiento;
  }

  guardar() {
    this.navCtrl.pop();
  }

  abrirCamara(tipo: string) {
    const that = this;
    let opciones = null;

    const album: CameraOptions = {
      quality: 100,
      sourceType: this.camara.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camara.MediaType.PICTURE,
      destinationType: this.camara.DestinationType.FILE_URI,
      encodingType: this.camara.EncodingType.JPEG,
      targetHeight: 400,
      targetWidth: 400,
      correctOrientation: true
    }

    const camara: CameraOptions = {
      quality: 100,
      sourceType: this.camara.PictureSourceType.CAMERA,
      mediaType: this.camara.MediaType.PICTURE,
      destinationType: this.camara.DestinationType.FILE_URI,
      encodingType: this.camara.EncodingType.JPEG,
      targetHeight: 400,
      targetWidth: 400,
      correctOrientation: true
    }

    tipo === 'camara' ? opciones = camara : opciones = album;

    this.camara.getPicture(opciones)
      .then(
      imageURI => {
        that.crop.crop(imageURI, { quality: 100 })
          .then(newImage => {
            let path = '';
            let pathComplete = newImage.split('/');
            let nombrePathFile = pathComplete.splice(pathComplete.length - 1)[0].replace("%20", " ");
            for (let item of pathComplete) path += item + `/`;


            that.file.readAsDataURL(path, nombrePathFile.split('?')[0])
              .then(base64Img => {

                /* No se cambia */
                let base64 = (<string>base64Img);
                let baseSinDecode = base64.split(',')[1];

                that.usuario.imagen = base64;
              });

          }).catch(err => { that.presentAlert(JSON.stringify(err)) });
      }).catch(err => { that.presentAlert(JSON.stringify(err)) });
  }

  presentAlert(mensaje) {
    let alert = this.alertCtrl.create({
      title: 'Sucedió algo..',
      subTitle: mensaje,
      buttons: ['Ok']
    });
    alert.present();


  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones',
      buttons: this.usuario.imagen ? [
        {
          text: 'Escoger de mi album',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'albums' : null,
          handler: () => {
            this.abrirCamara('album');
          }
        }, {
          text: 'Tomar una foto',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.abrirCamara('camara');
          }
        }, {
          text: 'Eliminar foto',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            //this.eliminarFoto();
          }
        }, {
          text: 'Cancelar',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ] :
        [
          {
            text: 'Escoger de mi album',
            role: 'destructive',
            icon: !this.platform.is('ios') ? 'albums' : null,
            handler: () => {
              this.abrirCamara('album');
            }
          }, {
            text: 'Tomar una foto',
            icon: !this.platform.is('ios') ? 'camera' : null,
            handler: () => {
              this.abrirCamara('camara');
            }
          }, {
            text: 'Cancelar',
            role: 'cancel',
            icon: !this.platform.is('ios') ? 'close' : null,
            handler: () => {
            }
          }
        ]
    });

    actionSheet.present();
  }


  presentLoadingDefault(mensaje = 'Cargando...') {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: mensaje
    });

    this.loading.present();
  }


  actualizarUsuario() {
    this.presentLoadingDefault();
    this.sesion.actualizarUsuario(this.email, this.nombres, this.apellidos, this.fecha).subscribe(data => {
      if (typeof data === 'string') {
        this.presentAlert(data);
      } else {
        console.log(data);
        this.navCtrl.pop();
      }

      this.loading.dismiss();
    });
  }
}
