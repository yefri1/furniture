import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductosProvider } from '../../providers/productos/productos';
import { DetalleCompraPage } from '../detalle-compra/detalle-compra';

@Component({
	selector: 'page-compras',
	templateUrl: 'compras.html',
})
export class ComprasPage {

	lista: any[];

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private productos: ProductosProvider) {

		this.listarCompras();
	}

	listarCompras() {
		this.productos.listarCompras().subscribe(data => {
			console.log(data);
			if (typeof data === 'string') {

			} else {
				this.lista = data;
			}

		});
	}

	cantidad(arr) {
		let c = 0;

		for (let item of arr) {
			c += item.cantidad;
		}

		return c;
	}


	doRefresh(refresher) {
		this.listarCompras();

		setTimeout(() => {
			refresher.complete();
		}, 2000);
	}

	detalleCompra(item) {
		this.navCtrl.push(DetalleCompraPage,{
			compra: item
		});
	}


}
