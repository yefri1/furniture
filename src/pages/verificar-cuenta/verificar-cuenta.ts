import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';
@Component({
	selector: 'page-verificar-cuenta',
	templateUrl: 'verificar-cuenta.html',
})
export class VerificarCuentaPage {

	email: string;
	code: string;
	errorMessage: string;
	callback: any;

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private sesion: SesionProvider, public alertCtrl: AlertController) {
		let email = this.navParams.get('email');
		this.email = email ? email : '';
	}

	ionViewDidLoad() {}

	close() {
		this.viewCtrl.dismiss();
	}

	verificarCuenta() {
		this.sesion.verificarCuenta(this.email, this.code).subscribe(
			data => {
				if (typeof data === 'string') {
					this.errorMessage = data
				} else {
					this.errorMessage = data.result
					this.presentAlert();
				}
			}
		)
	}

	presentAlert() {
		let alert = this.alertCtrl.create({
			title: 'Exitoso',
			subTitle: 'Cuenta confirmada exitosamente.',
			buttons: [
				{
					text: 'Ok',
					role: 'cancel',
					handler: () => {
						this.viewCtrl.dismiss(this.email);
					}
				}
			]
		});
		alert.present();
	}

}
