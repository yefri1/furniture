import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PagarPage } from '../pagar/pagar';

@Component({
	selector: 'page-metodo-envio',
	templateUrl: 'metodo-envio.html',
})

export class MetodoEnvioPage {

	direccion: string = 'dffdfd'
	calle: string;
	piso: string;

	tomorrow = new Date(new Date().getTime() + (48 * 60 * 60 * 1000)).toISOString().substring(0,10);
	

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		
	}

	next() {
		let params = {
			direccion: this.direccion,
			calle: this.calle,
			piso: this.piso
		}

		this.navCtrl.push(PagarPage, {
			'envio': params
		}, );
	}

}
