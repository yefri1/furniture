import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Util } from '../../recursos/util';
import { ProductosProvider } from '../../providers/productos/productos';
import { HomePage } from '../home/home';

@Component({
	selector: 'page-pagar',
	templateUrl: 'pagar.html',
})

export class PagarPage {
	
	envio: any;
	total: string;
	ahorro: string;

	loading: any;

	credicard = '';
	vencimiento: string;
	seguridad: string;

	save: number;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public recursos: Util,
		public productos: ProductosProvider,
		private alertCtrl: AlertController,
		public loadingCtrl: LoadingController) {
		this.envio = this.navParams.get('envio');



		this.totalEvento();
	}

	totalEvento() {
		let lista = this.recursos.obtenerListaProductos();
		let t = 0;
		let d = 0;

		for (let item of lista) {
			t += ((item.precio - (item.precio * (item.descuento / 100))) * item.cantidad);
			d += (item.precio * (item.descuento / 100) * item.cantidad);
		}

		this.total = 'S/.' + this.parseDecimal(t);
		this.ahorro = 'S/.' + this.parseDecimal(d);

		console.log(this.total);
	}

	parsePorcenaje(value) {

	}

	parseDecimal(num) {
		return parseFloat((Math.round(num * 100) / 100).toString()).toFixed(2);
	}

	registrarCompra() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));

		let lista = this.recursos.obtenerListaProductos();
		let p = [];

		for (let item of lista) {
			let e = {
				"idProducto": item.idProducto,
				"cantidad": item.cantidad
			}

			p.push(e);
		}

		this.presentLoading();
		this.productos.registrarCompra(this.envio.direccion, 'TARJETA', this.credicard, usuario.idUsuario, p).subscribe(
			data => {
				this.productos.enviarCorreo(data).subscribe(data => {
					console.log(data);
				});
				this.presentConfirm('Compra registrada exitosamente');

				this.loading.dismiss();
			}
		)
	}

	presentLoading() {
		this.loading = this.loadingCtrl.create({ content: 'Cargando espere...' });
		this.loading.present();

	}

	presentConfirm(mensaje) {
		let alert = this.alertCtrl.create({
			title: 'Exito',
			message: mensaje,
			buttons: [
				{
					text: 'Ok',
					role: 'cancel',
					handler: () => {

						this.navCtrl.push(HomePage, {
						});

						this.navCtrl.setRoot(HomePage, {
							params: 'goCompras'
						});

					}
				}
			]
		});
		alert.present();
	}

	cambio(value) {
		let parse = +value;

		if (value === 'e') {
			this.seguridad = '';
		}

		if (value.length === 3) {
			this.save = parse;
		} else if (value.length > 3) {
			this.seguridad = this.save.toString();
		} else {
			return;
		}
	}

	numerCard(value) {
		let cc = value.split("-").join("");

		cc = this.cc_format(cc);

		this.credicard = cc;
	}


	cc_format(value) {
		var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
		var matches = v.match(/\d{4,16}/g);
		var match = matches && matches[0] || ''
		var parts = []

		for (let i = 0, len = match.length; i < len; i += 4) {
			parts.push(match.substring(i, i + 4))
		}

		if (parts.length) {
			return parts.join(' ')
		} else {
			return value
		}
	}






}
