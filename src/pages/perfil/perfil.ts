import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ComprasPage } from '../compras/compras';
import { CambiarContrasenaPage } from '../cambiar-contrasena/cambiar-contrasena';
import { DetalleUsuarioPage } from '../detalle-usuario/detalle-usuario';


@Component({
	selector: 'page-perfil',
	templateUrl: 'perfil.html',
})
export class PerfilPage {

	user: any;
	callback: any;

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
		this.callback = this.navParams.get('callback');
		console.log(this.obtenerSesion());

		this.user = this.obtenerSesion();
	}

	obtenerSesion() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));
		
		return usuario ? usuario : {};
	}

	ionViewDidLeave() {
		this.callback(false).then(() => { });
	}


	ionViewDidLoad() {
		console.log('ionViewDidLoad PerfilPage');
	}

	salir() {
		localStorage.clear();
		this.navCtrl.pop();
	}

	compras() {
		this.navCtrl.push(ComprasPage, {
			
		}, );
	}

	editarPerfil() {
		this.navCtrl.push(DetalleUsuarioPage, {
			
		}, );
	}

	cambiarContrasena() {
		this.navCtrl.push(CambiarContrasenaPage, {
			
		
		}, );
	}



}
