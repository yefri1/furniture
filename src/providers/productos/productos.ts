import { HttpClient, HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductosProvider {

	BASEURLP = 'http://45.55.79.245:8080/testUp/';
	BASEAPI = 'http://furniture-api-node.herokuapp.com/api/messages';

	constructor(public http: HttpClient) {

	}

	getUser() {
		let usuario =  JSON.parse(localStorage.getItem('usuario'));

		return usuario ? usuario : {};
	}

	getMessageRandom() {
		return this.http.get(this.BASEAPI)
		.do(this.logResponse)
		.map(this.extractData)
		.catch(this.catchError)
	}

	listarForSale() {
		return this.http.get(`${this.BASEURLP}producto/getProductosForSale`)
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	listarForNow() {
		return this.http.get(`${this.BASEURLP}producto/getProductosNow`)
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	listarForPopular() {
		return this.http.get(`${this.BASEURLP}producto/getProductosPopular`)
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	listarCompras() {
		return this.http.post(`${this.BASEURLP}venta/listarCompraPorUsuario`,{
			"idUsuario": this.getUser().idUsuario
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}


	listarDetalleVenta(id) {
		return this.http.post(`${this.BASEURLP}venta/listarDetalleVenta`,{
			"idVenta": id
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	registrarCompra(direccion, tipoPago, nroTarjeta, idUsuario, listaProductos) {
		return this.http.post(`${this.BASEURLP}venta/registrarCompra`, {
			"entrega": {
				"direccion": direccion
			},
			"pago": {
				"tipo": tipoPago,
				"numeroTarjeta": nroTarjeta
			},
			"idUsuario": idUsuario,
			"detalle": listaProductos
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	enviarCorreo(data) {
		return this.http.post('http://furniture-api-node.herokuapp.com/api/mailcompra', data)
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	private catchError(error: any) {
		console.log(error);
		return "E";
	}

	private logResponse(res: any) {
		console.log(res);
	}

	private extractData(res: any) {
		return res
	}

}
