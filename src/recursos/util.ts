

export class Util {

    obtenerListaProductos() {
		let pro = localStorage.getItem('productos');
		let lista = pro ? JSON.parse(pro) : null;

		return lista;
	}

	eliminarProducto(item) {
		let productos: any[] = this.obtenerListaProductos();

		for (let e of productos) {
			if (item.idProducto === e.idProducto) {
				let a = productos.indexOf(e);
				productos.splice(a, 1);
			}
		}

		localStorage.setItem('productos', JSON.stringify(productos));
	}


	eliminaCarrito() {
		localStorage.removeItem('productos');
	}

	agregarProducto(item) {
		let productos = this.obtenerListaProductos();
		productos.push(item);
		localStorage.setItem('productos', JSON.stringify(productos));
	}

	actualizarProducto(item) {
		let productos: any[] = this.obtenerListaProductos();

		for (let e of productos) {
			if (item.idProducto === e.idProducto) {
				let a = productos.indexOf(e);
				productos[a] = item;
			}
		}

		localStorage.setItem('productos', JSON.stringify(productos));
	}
}