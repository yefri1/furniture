import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { File } from '@ionic-native/file';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetailsPage } from '../pages/details/details';
import { IniciarSesionPage } from '../pages/iniciar-sesion/iniciar-sesion';
import { VerificarCuentaPage } from '../pages/verificar-cuenta/verificar-cuenta';
import { PerfilPage } from '../pages/perfil/perfil';
import { CrearCuentaPage } from '../pages/crear-cuenta/crear-cuenta';
import { CarritoPage } from '../pages/carrito/carrito';
import { MetodoEnvioPage } from '../pages/metodo-envio/metodo-envio';
import { PagarPage } from '../pages/pagar/pagar';
import { ComprasPage } from '../pages/compras/compras';
import { ChatPage } from '../pages/chat/chat';
import { CambiarContrasenaPage } from '../pages/cambiar-contrasena/cambiar-contrasena';
import { DetalleCompraPage } from '../pages/detalle-compra/detalle-compra';
import { DetalleUsuarioPage } from '../pages/detalle-usuario/detalle-usuario';
import { SesionProvider } from '../providers/sesion/sesion';
import { ProductosProvider } from '../providers/productos/productos';
import { Util } from '../recursos/util';
import { LimitarTextoPipe } from '../pipes/limitar-texto/limitar-texto';

import { SocketIoConfig, SocketIoModule } from 'ng-socket-io';
import { XhrFactory } from '@angular/common/http/src/xhr';
const config: SocketIoConfig  = { url: 'http://45.55.79.245:4000/', options: {} };

@NgModule({
  declarations: [
    LimitarTextoPipe,
    MyApp,
    HomePage,
    DetailsPage,
    IniciarSesionPage,
    VerificarCuentaPage,
    PerfilPage,
    CrearCuentaPage,
    CarritoPage,
    CambiarContrasenaPage,
    MetodoEnvioPage,
    PagarPage,
    ComprasPage,
    ChatPage,
    DetalleCompraPage,
    DetalleUsuarioPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DetailsPage,
    IniciarSesionPage,
    VerificarCuentaPage,
    PerfilPage,
    CrearCuentaPage,
    CarritoPage,
    CambiarContrasenaPage,
    MetodoEnvioPage,
    PagarPage,
    ComprasPage,
    ChatPage,
    DetalleCompraPage,
    DetalleUsuarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Util,
    File,
    Camera,
    Crop,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SesionProvider,
    ProductosProvider
  ]
})


export class AppModule {}
